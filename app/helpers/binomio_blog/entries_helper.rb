module BinomioBlog
  module EntriesHelper

    def authorized_for_blog_edition?(user = current_user)
      BinomioBlog.authorized_for_edition? user
    end

  end
end