module BinomioBlog
  class CategoriesController < BinomioBlogController
    before_action do authorize_for_blog_edition end

    def index
      @categories = Category.all
    end

    def new
      category_name = "Nueva categoría"
      category_url_name = category_name.parameterize
      counter = 2
      while Category.exists?(name: category_name) || Category.exists?(url_name: category_url_name)
        category_name = "Nueva categoría #{counter}"
        category_url_name = category_name.parameterize
        counter += 1
      end
      Category.create!(name: category_name, url_name: category_url_name)
      redirect_to binomio_blog_categories_path
    end

    def destroy
      Category.find(params[:id]).destroy
      redirect_to binomio_blog_categories_path
    end
  end
end