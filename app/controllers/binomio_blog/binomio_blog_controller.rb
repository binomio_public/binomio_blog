module BinomioBlog
  # All the controllers within this gem inherit from this parent controller
  class BinomioBlogController < ApplicationController

    protected

    def authorize_for_blog_edition
      authenticate_user!
      redirect_to binomio_blog_path unless BinomioBlog.authorized_for_edition?(current_user)
    end

  end
end