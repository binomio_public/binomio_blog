module BinomioBlog
  class EntriesController < BinomioBlogController
    ENTRIES_PER_PAGE = 12 # When using infinite scroll, entries will be loaded in batches of this many
    before_action except: [:public_index, :show, :search_entries] do authorize_for_blog_edition end
    before_action only: [:show, :set_like] do set_entry end

    # The table of entries for administrators
    def admin_index
      @entries = Entry.all.order(published_at: :desc)
    end

    # The page that visitors see as the "home page" of the blog
    def public_index
    end

    # This method is called by public_index via AJAX. It returns a partial HTML containing the list of results, filtered by category,
    # and with a given offset, which is then injected into the DOM
    def search_entries
      offset = params[:offset].to_i
      entries = Entry.published.order(published_at: :desc).offset(offset).limit(ENTRIES_PER_PAGE)
      # Filter by category if applicable
      category = params[:category].present? ? Category.find_by_name(params[:category]) : nil
      entries = entries.where(category_id: category.id) if category.present?
      render partial: 'entry_list', locals: { entries: entries }
    end

    def new
      @entry = Entry.create!(
        status: 'draft',
        title: "Nueva entrada",
        content: "Contenido de la entrada aquí",
        author_name: "Autor",
        author_role: "Rol",
        view_count: 0,
        published_at: DateTime.now
      )
      redirect_to binomio_blog_entry_fancy_path(id: @entry.id, url_title: @entry.url_title)
    end

    def show
      if @entry.url_title != params[:url_title]
        redirect_to binomio_blog_entry_fancy_path(id: @entry.id, url_title: @entry.url_title)
      end
      if @entry.disabled? && !authorized_for_edition?
        redirect_to binomio_blog_path
      end
      unless user_signed_in?
        @entry.view_count += 1
        unless @entry.save
          puts "Failed to increase view count of BinomioBlog Entry: #{@entry.errors.full_messages[0]}"
        end
      end
    end

    # Permanently delete an Entry
    def destroy
      Entry.find(params[:id]).destroy
      redirect_to binomio_blog_entries_path
    end

    # Adds or removes a like from the requesting IP to the referenced Entry
    def set_like
      if params[:like] == 'true'
        @entry.add_like_by(request.ip)
      else
        @entry.remove_like_by(request.ip)
      end
      head 200
    end

    # Remove a like from the referenced Entry
    def unlike
      @entry.remove_like_by(request.ip)
      head 200
    end

    private

    def set_entry
      @entry = Entry.find params[:id]
    end
  end
end