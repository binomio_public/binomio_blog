module BinomioBlog
  class Entry < ActiveRecord::Base
    self.table_name = 'binomio_blog_entries'
    enum status: { published: 1, unlisted: 2, disabled: 3, draft: 4 }
    PRETTY_STATUSES = { published: 'Publicada', unlisted: 'Sin listar', disabled: 'Deshabilitada', draft: 'Borrador' }

    belongs_to :category, optional: true
    has_many :likes

    has_one_attached :head_image

    validates :status, presence: true
    validates :title, presence: true, length: { maximum: 128 }
    validates :content, presence: true, length: { maximum: 32768 }
    validates :author_name, presence: true, length: { maximum: 64 }
    validates :author_role, presence: true, length: { maximum: 64 }
    validates :view_count, presence: true
    validates :published_at, presence: true

    # Generate a string that matches the Entry's title but in a URL-safe way. This allows
    # us to append it to the Entry's URL for SEO improvement purposes.
    def url_title
      self.title.parameterize
    end

    # Given an IP address, add a like to this Entry unless the IP address has already
    # done so before. Returns true if the like was added, false otherwise.
    def add_like_by(ip_address)
      if likes.exists?(ip: ip_address)
        return false
      else
        Like.create!({entry_id: self.id, ip: ip_address})
        return true
      end
    end

    # If the given address has liked this Entry, remove the like
    def remove_like_by(ip_address)
      likes.where(ip: ip_address).delete_all
    end

    def like_count
      self.likes.count
    end

    def pretty_status
      PRETTY_STATUSES[self.status.to_sym] || "?"
    end

    def thumbnail
      if self.head_image.attached?
        self.head_image.variant(combine_options: {thumbnail: '600x400^', gravity: 'center', extent: '600x400'})
      else
        nil
      end
    end

    # Create a random blog entry. Creates a random amount of likes associated too.
    def self.create_random!
      entry = Entry.create!(
        category: Category.count > 0 ? Category.limit(1).order("RANDOM()").first : nil,
        status: 'published',
        title: Lipsum.words(2..12).capitalize,
        content: Lipsum.paragraphs(1..10),
        author_name: "John Doe",
        author_role: "Tester",
        view_count: rand(0..9999),
        published_at: rand(0..700).days.ago
      )
      rand(1..20).times do |i|
        entry.add_like_by "#{i}.#{rand(0..255)}.#{rand(0..255)}.#{rand(0..255)}"
      end
    end

  end
end
