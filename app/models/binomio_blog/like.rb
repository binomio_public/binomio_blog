module BinomioBlog
  class Like < ActiveRecord::Base
    self.table_name = 'binomio_blog_likes'

    belongs_to :entry
    validates :ip, presence: true, format: { with: /\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/ }
  end
end