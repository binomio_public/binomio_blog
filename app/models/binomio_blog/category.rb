module BinomioBlog
  class Category < ActiveRecord::Base
    self.table_name = 'binomio_blog_categories'

    has_many :entries
    validates :name, presence: true, length: { maximum: 64 }, uniqueness: true
    validates :url_name, presence: true, length: { maximum: 64 }, uniqueness: true, format: { with: /\A[a-z0-9-]+\z/ }
  end
end