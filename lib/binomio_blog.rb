require "binomio_blog/version"
require "binomio_blog/engine"
require "binomio_blog/lipsum"

module BinomioBlog

  # Given a User, return true if it should be allowed to administrate blog entries
  # By default all users can do so; the implementing project is encouraged to
  # override this method if stricter control is desired.
  def self.authorized_for_edition?(user)
    return user.present? && user.is_a?(User)
  end

  # Delete all blog-related data and replace it with randomized testing data
  def self.seed!
    Like.destroy_all
    Entry.destroy_all
    Category.destroy_all
    Category.create!(name: "Eventos", url_name: "eventos")
    Category.create!(name: "Promociones", url_name: "promociones")
    Category.create!(name: "Concursos", url_name: "concursos")
    Category.create!(name: "Anuncios", url_name: "anuncios")
    50.times do
      Entry.create_random!
    end
  end

end
