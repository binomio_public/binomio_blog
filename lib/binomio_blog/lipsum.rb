module BinomioBlog
  # This module simply creates random text. Useful to seed the database with random blog entries for testing.
  module Lipsum
    WORDS = ["lorem", "ipsum", "dolor", "sit", "amet", "consectetur", "adipiscing", "elit", "velit", "montes", "porttitor", "habitasse", "luctus", "quisque", "quam", "ridiculus", "aenean", "aptent", "gravida", "primis", "pharetra", "vitae", "cum", "facilisi", "nostra", "condimentum", "magnis", "rutrum", "morbi", "netus", "massa", "nec", "dignissim", "penatibus", "sodales", "natoque", "sapien", "lacinia", "sagittis", "odio", "lectus", "bibendum", "fringilla", "nisi", "fames", "varius", "curae", "nulla", "erat", "mus", "potenti", "habitant", "duis", "dapibus", "tincidunt", "viverra", "ut", "malesuada", "litora", "fermentum", "quis", "dictumst", "augue", "mi", "sed", "commodo", "consequat", "proin", "per", "dis", "lacus", "platea", "vel", "at", "facilisis", "eget", "tempus", "phasellus", "ac", "tempor", "lobortis", "risus", "porta", "suscipit", "euismod", "ultrices", "class", "nullam", "taciti", "congue", "sociis", "nascetur", "cubilia", "dictum", "himenaeos", "suspendisse", "volutpat", "elementum", "pretium", "hendrerit", "aliquam", "vulputate", "neque", "pulvinar", "maecenas", "et", "molestie", "curabitur", "senectus", "magna", "ligula", "ad", "integer", "diam", "laoreet", "cursus", "interdum", "mollis", "placerat", "nibh", "eu", "parturient", "ornare", "pellentesque", "arcu", "leo", "vivamus", "in", "eros", "orci", "a", "torquent", "sem", "scelerisque", "faucibus", "semper", "vehicula", "feugiat", "nam", "imperdiet", "posuere", "donec", "conubia", "non", "ullamcorper", "aliquet", "eleifend", "mauris", "accumsan", "mattis", "tristique", "justo", "est", "nunc", "auctor", "cras", "metus", "fusce", "urna", "ante", "dui", "ultricies", "iaculis", "felis", "enim", "turpis", "id", "egestas", "convallis", "nisl", "blandit", "libero", "venenatis", "sociosqu", "hac", "etiam", "praesent", "inceptos", "sollicitudin", "tortor", "purus", "tellus", "rhoncus", "vestibulum"]

    def self.words(amount = 1)
      amount = rand(amount) if amount.is_a? Range
      raise "Maximum of #{WORDS.length} exceeded" if amount > WORDS.length
      WORDS.sample(amount).join(" ")
    end

    def self.sentences(amount = 1)
      amount = rand(amount) if amount.is_a? Range
      (1..amount).map{|n| "#{words(rand(5..25)).capitalize}."}.join(" ")
    end

    def self.paragraphs(amount = 1)
      amount = rand(amount) if amount.is_a? Range
      (1..amount).map{|n| "<p>#{sentences(rand(1..10))}</p>"}.join("\n")
    end
  end
end