class CreateBinomioBlogTables < ActiveRecord::Migration[5.2]
  def change
    create_table :binomio_blog_entries do |t|
      t.integer :category_id
      t.integer :status
      t.string :title
      t.text :content
      t.string :author_name
      t.string :author_role
      t.integer :view_count
      t.datetime :published_at
      t.timestamps
    end
    create_table :binomio_blog_categories do |t|
      t.string :name
      t.string :url_name
    end
    create_table :binomio_blog_likes do |t|
      t.integer :entry_id
      t.string :ip
      t.index [:entry_id, :ip]
    end
  end
end
