require 'thor/group'
module BinomioBlog
  module Generators
    class Views < Thor::Group
      include Thor::Actions

      # This defines where Thor looks for files to copy
      def self.source_root
        File.expand_path("../../../../", __FILE__)
      end

      def copy_views
        ['admin_index', 'help', 'public_index', 'show', '_entry_list'].each do |name|
          copy_file("app/views/binomio_blog/entries/#{name}.html.erb", "app/views/binomio_blog/entries/#{name}.html.erb")
        end
        ['index'].each do |name|
          copy_file("app/views/binomio_blog/categories/#{name}.html.erb", "app/views/binomio_blog/categories/#{name}.html.erb")
        end
      end

      def copy_assets
        copy_file("app/assets/images/binomio_blog/default_thumbnail.jpg", "app/assets/images/binomio_blog/default_thumbnail.jpg")
      end
    end
  end
end
