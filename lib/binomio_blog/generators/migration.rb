require 'thor/group'
module BinomioBlog
  module Generators
    class Migration < Thor::Group
      include Thor::Actions

      # This defines where Thor looks for files to copy
      def self.source_root
        File.dirname(__FILE__) + "/templates"
      end

      # Copy the migration file
      def create_migration
        copy_file("create_binomio_blog_tables.rb", "db/migrate/#{Time.now.strftime("%Y%m%d%H%M%S")}_create_binomio_blog_tables.rb")
      end
    end
  end
end
