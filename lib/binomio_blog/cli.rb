require 'thor'
require 'binomio_blog/generators/migration'
require 'binomio_blog/generators/views'

module BinomioBlog
  class CLI < Thor

    desc "migrations", "Generate the migration that creates the tables required by the gem's models"
    def migration
      BinomioBlog::Generators::Migration.start
    end

    desc "views", "Generate the views with the skeleton for you to customize"
    def views
      BinomioBlog::Generators::Views.start
    end

    desc "install", "Generate all the required files"
    def install
      BinomioBlog::Generators::Migration.start
      BinomioBlog::Generators::Views.start
    end
  end
end
