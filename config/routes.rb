Rails.application.routes.draw do
  # ---------------------------- ENTRIES CONTROLLER ----------------------------
  get 'blog' => 'binomio_blog/entries#public_index', as: 'binomio_blog'
  get 'blog/entradas' => 'binomio_blog/entries#admin_index', as: 'binomio_blog_entries'
  get 'blog/entradas/buscar' => 'binomio_blog/entries#search_entries', as: 'binomio_blog_search'
  get 'blog/entradas/nueva' => 'binomio_blog/entries#new', as: 'binomio_blog_new_entry'
  get 'blog/entradas/:id' => 'binomio_blog/entries#show', as: 'binomio_blog_entry'
  delete 'blog/entradas/:id' => 'binomio_blog/entries#destroy'
  get 'blog/entradas/:id/:url_title' => 'binomio_blog/entries#show', as: 'binomio_blog_entry_fancy'
  post 'blog/entradas/:id/set_like' => 'binomio_blog/entries#set_like', as: 'binomio_blog_set_like'
  get 'blog/ayuda' => 'binomio_blog/entries#help', as: 'binomio_blog_help'

  # ---------------------------- CATEGORIES CONTROLLER ----------------------------
  get 'blog/categorias' => 'binomio_blog/categories#index', as: 'binomio_blog_categories'
  get 'blog/categorias/nueva' => 'binomio_blog/categories#new', as: 'binomio_blog_new_category'
  delete 'blog/categorias/:id' => 'binomio_blog/categories#destroy', as: 'binomio_blog_category'
end